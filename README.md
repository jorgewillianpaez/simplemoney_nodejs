<div align="center">
    <h1>Simpl&Money</h1>
    <h4>Backend service API for Simpl&Money app<h4>
    <img src="https://i0.wp.com/projectsplaza.com/wp-content/uploads/2018/02/create-simple-static-website-with-nodejs-express.png?resize=930%2C400&ssl=1" alt="Node and express image">
</div>

## Setup

* Clone the repository & `cd` to the root directory.

* Install the dependencies with the command bellow:

```
yarn install
```

## Run

To run the application in the development environment, you need to use the command bellow:

```
yarn dev
```

The application will run locally in port `3000`.

## Run Tests

To run all the application tests, you need to use the command bellow:

```
yarn test:ci
```

## Remarks

This template is developed and tested on

- Node v16.13.1
- Ubuntu 20.04.3 LTS