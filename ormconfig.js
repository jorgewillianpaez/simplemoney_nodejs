const devEnv = {
  type: 'postgres',
  url: process.env.DB_URL,
  ssl: process.env.NODE_ENV === 'production'
    ? { rejectUnauthorized: false }
    : false,
  entities: process.env.NODE_ENV === 'production'
    ? ['./dist/entities/**/*.js']
    : ['./src/entities/**/*.ts'],
  migrations: process.env.NODE_ENV === 'production'
    ? ['./dist/database/migrations/*.js']
    : ['./src/database/migrations/*.ts'],
  cli: {
    migrationsDir: './src/database/migrations'
  },
  logging: true,
  synchronize: false
}

module.exports = devEnv
