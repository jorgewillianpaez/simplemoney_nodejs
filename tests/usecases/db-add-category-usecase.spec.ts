import { DbAddCategory } from '../../src/usecases/db-add-category-usecase'
import { AddCategoryRepositorySpy, CheckCategoryByNameRepositorySpy, mockAddCategoryParams } from '../mocks/mock-category'
import { throwError } from '../mocks/throw-error'

type SutTypes = {
  sut: DbAddCategory
  addCategoryRepositorySpy: AddCategoryRepositorySpy
  checkCategoryByNameRepositorySpy: CheckCategoryByNameRepositorySpy
}

const makeSut = (): SutTypes => {
  const addCategoryRepositorySpy = new AddCategoryRepositorySpy()
  const checkCategoryByNameRepositorySpy = new CheckCategoryByNameRepositorySpy()
  const sut = new DbAddCategory(addCategoryRepositorySpy, checkCategoryByNameRepositorySpy)
  return {
    sut,
    addCategoryRepositorySpy,
    checkCategoryByNameRepositorySpy
  }
}

describe('DbAddCategory Usecase', () => {
  it('Should call AddCategoryRepository with correct values', async () => {
    const { sut, addCategoryRepositorySpy } = makeSut()
    const addCategoryParams = mockAddCategoryParams()
    await sut.add(addCategoryParams)
    expect(addCategoryRepositorySpy.params).toEqual({
      name: addCategoryParams.name,
      description: addCategoryParams.description
    })
  })

  it('Should throw an error if AddCategoryRepository throws', async () => {
    const { sut, addCategoryRepositorySpy } = makeSut()
    jest.spyOn(addCategoryRepositorySpy, 'add').mockImplementationOnce(throwError)
    const addCategoryParams = mockAddCategoryParams()
    const promise = sut.add(addCategoryParams)
    await expect(promise).rejects.toThrow()
  })

  it('Should return true on success', async () => {
    const { sut } = makeSut()
    const addCategoryParams = mockAddCategoryParams()
    const isValid = await sut.add(addCategoryParams)
    expect(isValid).toBe(true)
  })

  it('Should return false if AddCategoryRepository returns false', async () => {
    const { sut, addCategoryRepositorySpy } = makeSut()
    addCategoryRepositorySpy.result = false
    const addCategoryParams = mockAddCategoryParams()
    const isValid = await sut.add(addCategoryParams)
    expect(isValid).toBe(false)
  })

  it('Should return false if CheckCategoryByNameRepository returns true', async () => {
    const { sut, checkCategoryByNameRepositorySpy } = makeSut()
    checkCategoryByNameRepositorySpy.result = true
    const addCategoryParams = mockAddCategoryParams()
    const isValid = await sut.add(addCategoryParams)
    expect(isValid).toBe(false)
  })

  it('Should call CheckCategoryByNameRepository with correct name', async () => {
    const { sut, checkCategoryByNameRepositorySpy } = makeSut()
    const addCategoryParams = mockAddCategoryParams()
    await sut.add(addCategoryParams)
    expect(checkCategoryByNameRepositorySpy.name).toBe(addCategoryParams.name)
  })
})
