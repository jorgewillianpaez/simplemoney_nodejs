import faker from 'faker'

import { DbDeleteCategory } from '../../src/usecases/db-delete-category-usecase'
import { DeleteCategoryRepositorySpy } from '../mocks/mock-category'
import { throwError } from '../mocks/throw-error'

type SutTypes = {
  sut: DbDeleteCategory
  deleteCategoryRepositorySpy: DeleteCategoryRepositorySpy
}

const makeSut = (): SutTypes => {
  const deleteCategoryRepositorySpy = new DeleteCategoryRepositorySpy()
  const sut = new DbDeleteCategory(deleteCategoryRepositorySpy)
  return {
    sut,
    deleteCategoryRepositorySpy
  }
}

describe('DbDeleteCategory Usecase', () => {
  it('Should call DeleteCategoryRepository', async () => {
    const { sut, deleteCategoryRepositorySpy } = makeSut()
    const categoryId = faker.datatype.uuid()
    await sut.delete(categoryId)
    expect(deleteCategoryRepositorySpy.categoryId).toBe(categoryId)
  })

  it('Should throw if DeleteCategoryRepository throws', async () => {
    const { sut, deleteCategoryRepositorySpy } = makeSut()
    jest.spyOn(deleteCategoryRepositorySpy, 'delete').mockImplementationOnce(throwError)
    const categoryId = faker.datatype.uuid()
    const promise = sut.delete(categoryId)
    await expect(promise).rejects.toThrow()
  })

  it('Should return true if DeleteCategoryRepository returns true', async () => {
    const { sut, deleteCategoryRepositorySpy } = makeSut()
    deleteCategoryRepositorySpy.result = true
    const categoryId = faker.datatype.uuid()
    const result = await sut.delete(categoryId)
    expect(result).toBe(true)
  })

  it('Should return false if DeleteCategoryRepository returns false', async () => {
    const { sut, deleteCategoryRepositorySpy } = makeSut()
    deleteCategoryRepositorySpy.result = false
    const categoryId = faker.datatype.uuid()
    const result = await sut.delete(categoryId)
    expect(result).toBe(false)
  })
})
