import { DbLoadCategories } from '../../src/usecases/db-load-categories-usecase'
import { LoadCategoriesRepositorySpy } from '../mocks/mock-category'
import { throwError } from '../mocks/throw-error'

type SutTypes = {
  sut: DbLoadCategories
  loadCategoriesRepositorySpy: LoadCategoriesRepositorySpy
}

const makeSut = (): SutTypes => {
  const loadCategoriesRepositorySpy = new LoadCategoriesRepositorySpy()
  const sut = new DbLoadCategories(loadCategoriesRepositorySpy)
  return {
    sut,
    loadCategoriesRepositorySpy
  }
}

describe('DbLoadCategories Usecase', () => {
  it('Should call LoadCategoriesRepository', async () => {
    const { sut, loadCategoriesRepositorySpy } = makeSut()
    const isAdm = true
    await sut.load(isAdm)
    expect(loadCategoriesRepositorySpy.isAdm).toBe(isAdm)
  })

  it('Should return a list of Categories on success', async () => {
    const { sut, loadCategoriesRepositorySpy } = makeSut()
    const isAdm = true
    const categories = await sut.load(isAdm)
    expect(categories).toEqual(loadCategoriesRepositorySpy.result)
  })

  it('Should throw if LoadCategoriesRepository throws', async () => {
    const { sut, loadCategoriesRepositorySpy } = makeSut()
    jest.spyOn(loadCategoriesRepositorySpy, 'loadAll').mockImplementationOnce(throwError)
    const isAdm = true
    const promise = sut.load(isAdm)
    await expect(promise).rejects.toThrow()
  })

  it('Should return undefined if user is not an Administrator', async () => {
    const { sut } = makeSut()
    const isAdm = false
    const categories = await sut.load(isAdm)
    expect(categories).toEqual(undefined)
  })
})
