import { makeCreateCategoryValidation } from '../../../src/factories/controllers/create-category-validation-factory'
import { Validation } from '../../../src/protocols'
import { RequiredFieldValidation, ValidationComposite } from '../../../src/validation'

jest.mock('@../../../src/validation/validation-composite')

describe('CreateCategoryValidation Factory', () => {
  it('Should call ValidationComposite with all validations', () => {
    makeCreateCategoryValidation()
    const validations: Validation[] = []
    for (const field of ['name', 'description']) {
      validations.push(new RequiredFieldValidation(field))
    }
    expect(ValidationComposite).toHaveBeenCalledWith(validations)
  })
})
