import faker from 'faker'
import { Category } from '../../src/entities'
import { AddCategoryRepository } from '../../src/repositories/add-category-repository'
import { CheckCategoryByNameRepository } from '../../src/repositories/check-category-by-name-repository'
import { DeleteCategoryRepository } from '../../src/repositories/delete-category-repository'
import { LoadCategoriesRepository } from '../../src/repositories/load-categories-repository'
import { AddCategory } from '../../src/usecases/add-category-usecase'
import { DeleteCategory } from '../../src/usecases/delete-category-usecase'
import { LoadCategories } from '../../src/usecases/load-categories-usecase'

export class AddCategorySpy implements AddCategory {
  params: AddCategory.Params
  result = true

  async add (category: AddCategory.Params): Promise<AddCategory.Result> {
    this.params = category
    return this.result
  }
}

export const mockAddCategoryParams = (): AddCategory.Params => ({
  name: faker.random.word(),
  description: faker.random.words()
})

export const mockCategory = (): Category => {
  return {
    id: faker.datatype.uuid(),
    name: faker.random.word(),
    description: faker.random.words()
  }
}

export const mockCategories = (): Category[] => [
  mockCategory(),
  mockCategory(),
  mockCategory(),
  mockCategory()
]

export class AddCategoryRepositorySpy implements AddCategoryRepository {
  params: AddCategoryRepository.Params
  result = true

  async add (params: AddCategoryRepository.Params): Promise<AddCategoryRepository.Result> {
    this.params = params
    return this.result
  }
}

export class CheckCategoryByNameRepositorySpy implements CheckCategoryByNameRepository {
  name: string
  result = false

  async checkByName (name: string): Promise<CheckCategoryByNameRepository.Result> {
    this.name = name
    return this.result
  }
}

export class LoadCategoriesSpy implements LoadCategories {
  result: Category[] | undefined = mockCategories()
  async load (): Promise<LoadCategories.Result> {
    return this.result
  }
}

export class LoadCategoriesRepositorySpy implements LoadCategoriesRepository {
  isAdm: boolean
  result = mockCategories()

  async loadAll (isAdm: boolean): Promise<LoadCategoriesRepository.Result> {
    this.isAdm = isAdm
    if (!this.isAdm) return undefined
    return this.result
  }
}

export class DeleteCategorySpy implements DeleteCategory {
  result = true

  async delete (categoryId: string): Promise<boolean> {
    return this.result
  }
}

export class DeleteCategoryRepositorySpy implements DeleteCategoryRepository {
  categoryId: string
  result = true

  async delete (categoryId: string): Promise<DeleteCategoryRepository.Result> {
    this.categoryId = categoryId
    return this.result
  }
}
