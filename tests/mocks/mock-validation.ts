import { Validation } from '../../src/protocols'

export class ValidationSpy implements Validation {
  input: any
  error: any

  validate (input: any): Error | undefined {
    this.input = input
    return this.error
  }
}
