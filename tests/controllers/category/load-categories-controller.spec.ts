import { LoadCategoriesController } from '../../../src/controllers/category/load-categories-controller'
import { AdminRequiredError } from '../../../src/errors/admin-required-error'
import { forbidden, noContent, ok, serverError } from '../../../src/helpers'
import { LoadCategoriesSpy } from '../../mocks/mock-category'
import { throwError } from '../../mocks/throw-error'

type SutTypes = {
  sut: LoadCategoriesController
  loadCategoriesSpy: LoadCategoriesSpy
}

const makeSut = (): SutTypes => {
  const loadCategoriesSpy = new LoadCategoriesSpy()
  const sut = new LoadCategoriesController(loadCategoriesSpy)
  return {
    sut,
    loadCategoriesSpy
  }
}

describe('LoadCategories Controller', () => {
  it('Should return 200 on success', async () => {
    const { sut, loadCategoriesSpy } = makeSut()
    const httpResponse = await sut.execute({})
    expect(httpResponse).toEqual(ok(loadCategoriesSpy.result))
  })

  it('Should return 204 if LoadCategories returns empty', async () => {
    const { sut, loadCategoriesSpy } = makeSut()
    loadCategoriesSpy.result = []
    const httpResponse = await sut.execute({})
    expect(httpResponse).toEqual(noContent())
  })

  it('Should return 500 if LoadCategories throws', async () => {
    const { sut, loadCategoriesSpy } = makeSut()
    jest.spyOn(loadCategoriesSpy, 'load').mockImplementationOnce(throwError)
    const httpResponse = await sut.execute({})
    expect(httpResponse).toEqual(serverError(new Error()))
  })

  it('Should return 403 if LoadCategories return undefined', async () => {
    const { sut, loadCategoriesSpy } = makeSut()
    loadCategoriesSpy.result = undefined
    const httpResponse = await sut.execute({})
    expect(httpResponse).toEqual(forbidden(new AdminRequiredError()))
  })
})
