import { throwError } from '../../mocks/throw-error'

import faker from 'faker'
import { forbidden, noContent, notFound, serverError } from '../../../src/helpers'
import { ServerError } from '../../../src/errors'
import { DeleteCategorySpy } from '../../mocks/mock-category'
import { DeleteCategoryController } from '../../../src/controllers/category/delete-category-controller'
import { CategoryNotFoundError } from '../../../src/errors/category-not-found-error'
import { AdminRequiredError } from '../../../src/errors/admin-required-error'

const mockRequest = (): any => ({
  uuid: faker.datatype.uuid(),
  isAdm: true
})

type SutTypes = {
  sut: DeleteCategoryController
  deleteCategorySpy: DeleteCategorySpy
}

const makeSut = (): SutTypes => {
  const deleteCategorySpy = new DeleteCategorySpy()
  const sut = new DeleteCategoryController(deleteCategorySpy)
  return {
    sut,
    deleteCategorySpy
  }
}

describe('DeleteCategory Controller', () => {
  it('Should return 500 if DeleteCategory throws', async () => {
    const { sut, deleteCategorySpy } = makeSut()
    jest.spyOn(deleteCategorySpy, 'delete').mockImplementationOnce(throwError)
    const httpRequest = mockRequest()
    const httpResponse = await sut.execute(httpRequest)
    expect(httpResponse).toEqual(serverError(new ServerError('Internal Error')))
  })

  it('Should return 200 on success', async () => {
    const { sut } = makeSut()
    const httpRequest = mockRequest()
    const httpResponse = await sut.execute(httpRequest)
    expect(httpResponse).toEqual(noContent())
  })

  it('Should return 404 if DeleteCategory returns false', async () => {
    const { sut, deleteCategorySpy } = makeSut()
    deleteCategorySpy.result = false
    const httpRequest = mockRequest()
    const httpResponse = await sut.execute(httpRequest)
    expect(httpResponse).toEqual(notFound(new CategoryNotFoundError()))
  })

  it('Should return 403 if user is not an Administrator', async () => {
    const { sut } = makeSut()
    const httpRequest = mockRequest()
    httpRequest.isAdm = false
    const httpResponse = await sut.execute(httpRequest)
    expect(httpResponse).toEqual(forbidden(new AdminRequiredError()))
  })
})
