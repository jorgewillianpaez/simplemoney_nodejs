import faker from 'faker'
import { CreateCategoryController } from '../../../src/controllers/category/create-category-controller'
import { ServerError } from '../../../src/errors'
import { AdminRequiredError } from '../../../src/errors/admin-required-error'
import { NameExistsError } from '../../../src/errors/name-exists-error'
import { badRequest, forbidden, ok, serverError } from '../../../src/helpers'
import { AddCategorySpy } from '../../mocks/mock-category'
import { ValidationSpy } from '../../mocks/mock-validation'
import { throwError } from '../../mocks/throw-error'

const mockRequest = (): any => ({
  name: faker.random.word(),
  description: faker.random.words(),
  isAdm: true
})

type SutTypes = {
  sut: CreateCategoryController
  validationSpy: ValidationSpy
  addCategorySpy: AddCategorySpy
}

const makeSut = (): SutTypes => {
  const validationSpy = new ValidationSpy()
  const addCategorySpy = new AddCategorySpy()
  const sut = new CreateCategoryController(validationSpy, addCategorySpy)
  return {
    sut,
    validationSpy,
    addCategorySpy
  }
}

describe('CreateCategory Controller', () => {
  it('Should call Validation with correct values', async () => {
    const { sut, validationSpy } = makeSut()
    const request = mockRequest()
    await sut.execute(request)
    expect(validationSpy.input).toEqual(request)
  })

  it('Should return 400 if Validation fails validate', async () => {
    const { sut, validationSpy } = makeSut()
    validationSpy.error = new Error()
    const request = mockRequest()
    const httpResponse = await sut.execute(request)
    expect(httpResponse).toEqual(badRequest(validationSpy.error))
  })

  it('Should return 500 if AddCategory throws', async () => {
    const { sut, addCategorySpy } = makeSut()
    const request = mockRequest()
    jest.spyOn(addCategorySpy, 'add').mockImplementationOnce(throwError)
    const httpResponse = await sut.execute(request)
    expect(httpResponse).toEqual(serverError(new ServerError('Internal Error')))
  })

  it('Should call AddCategory with correct values', async () => {
    const { sut, addCategorySpy } = makeSut()
    const request = mockRequest()
    await sut.execute(request)
    expect(addCategorySpy.params).toEqual({
      name: request.name,
      description: request.description
    })
  })

  it('Should return 403 if AddCategory returns false', async () => {
    const { sut, addCategorySpy } = makeSut()
    addCategorySpy.result = false
    const request = mockRequest()
    const httpResponse = await sut.execute(request)
    expect(httpResponse).toEqual(forbidden(new NameExistsError()))
  })

  it('Should return 200 if valid data is provided', async () => {
    const { sut } = makeSut()
    const request = mockRequest()
    const httpResponse = await sut.execute(request)
    expect(httpResponse).toEqual(ok({
      message: `Category ${request.name} added to database`
    }))
  })

  it('Should return 403 if the user is not an Administrator', async () => {
    const { sut } = makeSut()
    const request = mockRequest()
    request.isAdm = false
    const httpResponse = await sut.execute(request)
    expect(httpResponse).toEqual(forbidden(new AdminRequiredError()))
  })
})
