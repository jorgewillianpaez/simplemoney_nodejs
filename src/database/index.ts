import { ConnectionOptions } from 'typeorm'
import * as dotenv from 'dotenv'

dotenv.config()

const config: ConnectionOptions = {
  type: 'postgres',
  url: process.env.DB_URL,
  entities: ['./src/entities/**/*.ts'],
  migrations: ['./src/database/migrations/*.ts'],
  cli: {
    migrationsDir: './src/database/migrations'
  },
  synchronize: true,
  logging: true
}

export default config
