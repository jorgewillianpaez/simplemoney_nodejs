import { MigrationInterface, QueryRunner } from 'typeorm'

export class creatingTables1645794444312 implements MigrationInterface {
  name = 'creatingTables1645794444312'

  public async up (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'CREATE TABLE "hints" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "body" character varying NOT NULL, "references" character varying NOT NULL, "type" character varying NOT NULL, "user_email" character varying NOT NULL, "userId" uuid, CONSTRAINT "PK_3a6a970be4cdfcb0d7f0be2bd01" PRIMARY KEY ("id"))'
    )
    await queryRunner.query(
      'CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "cpf" character varying, "email" character varying NOT NULL, "password" character varying NOT NULL, "phone" character varying, "role" character varying NOT NULL, "subscription" character varying NOT NULL, "image_url" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "UQ_8e1f623798118e629b46a9e6299" UNIQUE ("phone"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))'
    )
    await queryRunner.query(
      'CREATE TABLE "expenses" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "value" double precision, "quota" integer NOT NULL DEFAULT \'1\', "actual_quota" integer NOT NULL, "type" character varying, "category" character varying, "accountId" uuid, CONSTRAINT "PK_94c3ceb17e3140abc9282c20610" PRIMARY KEY ("id"))'
    )
    await queryRunner.query(
      'CREATE TABLE "loans" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "payments" character varying NOT NULL, "amortizations" character varying NOT NULL, "interests" character varying NOT NULL, "balances" character varying NOT NULL, CONSTRAINT "PK_5c6942c1e13e4de135c5203ee61" PRIMARY KEY ("id"))'
    )
    await queryRunner.query(
      'CREATE TABLE "portfolios" ("name" SERIAL NOT NULL, "profileType" character varying NOT NULL, "stocks" character varying NOT NULL, "objective" character varying NOT NULL, CONSTRAINT "PK_29f7b2bcfa0d26fa6699015037f" PRIMARY KEY ("name"))'
    )
    await queryRunner.query(
      'CREATE TABLE "categories" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_24dbc6126a28ff948da33e97d3b" PRIMARY KEY ("id"))'
    )
    await queryRunner.query(
      'CREATE TABLE "accounts" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "balance" integer NOT NULL, "user_email" character varying NOT NULL, "userId" uuid, CONSTRAINT "PK_5a7a02c20412299d198e097a8fe" PRIMARY KEY ("id"))'
    )
    await queryRunner.query(
      'ALTER TABLE "hints" ADD CONSTRAINT "FK_7ce16aca60e3745cbf61f522141" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION'
    )
    await queryRunner.query(
      'ALTER TABLE "expenses" ADD CONSTRAINT "FK_0877e9901d5296c2e7c7f59b018" FOREIGN KEY ("accountId") REFERENCES "accounts"("id") ON DELETE NO ACTION ON UPDATE NO ACTION'
    )
    await queryRunner.query(
      'ALTER TABLE "accounts" ADD CONSTRAINT "FK_3aa23c0a6d107393e8b40e3e2a6" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION'
    )
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE "accounts" DROP CONSTRAINT "FK_3aa23c0a6d107393e8b40e3e2a6"'
    )
    await queryRunner.query(
      'ALTER TABLE "expenses" DROP CONSTRAINT "FK_0877e9901d5296c2e7c7f59b018"'
    )
    await queryRunner.query(
      'ALTER TABLE "hints" DROP CONSTRAINT "FK_7ce16aca60e3745cbf61f522141"'
    )
    await queryRunner.query('DROP TABLE "accounts"')
    await queryRunner.query('DROP TABLE "categories"')
    await queryRunner.query('DROP TABLE "portfolios"')
    await queryRunner.query('DROP TABLE "loans"')
    await queryRunner.query('DROP TABLE "expenses"')
    await queryRunner.query('DROP TABLE "user"')
    await queryRunner.query('DROP TABLE "hints"')
  }
}
