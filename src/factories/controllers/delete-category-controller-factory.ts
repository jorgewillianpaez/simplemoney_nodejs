import { DeleteCategoryController } from '../../controllers/category/delete-category-controller'
import { Controller } from '../../protocols'
import { makeDbDeleteCategory } from '../usecases/delete-category-factory'

export const makeDeleteCategoryController = (): Controller => {
  return new DeleteCategoryController(makeDbDeleteCategory())
}
