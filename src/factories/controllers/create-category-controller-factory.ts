import { CreateCategoryController } from '../../controllers/category/create-category-controller'
import { Controller } from '../../protocols'
import { makeDbAddCategory } from '../usecases/add-category-factory'
import { makeCreateCategoryValidation } from './create-category-validation-factory'

export const makeCreateCategoryController = (): Controller => {
  return new CreateCategoryController(makeCreateCategoryValidation(), makeDbAddCategory())
}
