import { LoadCategoriesController } from '../../controllers/category/load-categories-controller'
import { Controller } from '../../protocols'
import { makeDbLoadCategories } from '../usecases/load-categories-factory'

export const makeLoadCategoriesController = (): Controller => {
  return new LoadCategoriesController(makeDbLoadCategories())
}
