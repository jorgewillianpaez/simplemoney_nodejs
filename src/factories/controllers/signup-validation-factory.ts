import { Validation } from '../../protocols'
import { RequiredFieldValidation, ValidationComposite } from '../../validation'

export const makeSignUpValidation = (): ValidationComposite => {
  const validations: Validation[] = []
  for (const field of ['name', 'cpf', 'email', 'password', 'phone', 'role', 'subscription', 'imageUrl']) {
    validations.push(new RequiredFieldValidation(field))
  }
  return new ValidationComposite(validations)
}
