import { Validation } from '../../protocols'
import { RequiredFieldValidation, ValidationComposite } from '../../validation'

export const makeCreateCategoryValidation = (): ValidationComposite => {
  const validations: Validation[] = []
  for (const field of ['name', 'description']) {
    validations.push(new RequiredFieldValidation(field))
  }
  return new ValidationComposite(validations)
}
