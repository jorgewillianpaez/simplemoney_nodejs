import { SignUpController } from '../../controllers/user/signup-controller-v2'
import { Controller } from '../../protocols'
import { makeDbAddUser } from '../usecases/add-user-factory'
import { makeSignUpValidation } from './signup-validation-factory'

export const makeSignUpController = (): Controller => {
  return new SignUpController(makeSignUpValidation(), makeDbAddUser())
}
