import { CategoryTypeORMRepository } from '../../repositories/category-typeorm-repository'
import { DbLoadCategories } from '../../usecases/db-load-categories-usecase'
import { LoadCategories } from '../../usecases/load-categories-usecase'

export const makeDbLoadCategories = (): LoadCategories => {
  const categoryTypeORMRepository = new CategoryTypeORMRepository()
  return new DbLoadCategories(categoryTypeORMRepository)
}
