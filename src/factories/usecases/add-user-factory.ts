import { UserTypeORMRepository } from '../../repositories/user-typeorm-repository'
import { AddUser } from '../../usecases/add-user-usecase'
import { DbAddUser } from '../../usecases/db-add-user-usecase'

export const makeDbAddUser = (): AddUser => {
  const userTypeORMRepository = new UserTypeORMRepository()
  return new DbAddUser(userTypeORMRepository, userTypeORMRepository)
}
