import { CategoryTypeORMRepository } from '../../repositories/category-typeorm-repository'
import { DbDeleteCategory } from '../../usecases/db-delete-category-usecase'
import { DeleteCategory } from '../../usecases/delete-category-usecase'

export const makeDbDeleteCategory = (): DeleteCategory => {
  const categoryTypeORMRepository = new CategoryTypeORMRepository()
  return new DbDeleteCategory(categoryTypeORMRepository)
}
