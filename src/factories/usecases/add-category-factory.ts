import { CategoryTypeORMRepository } from '../../repositories/category-typeorm-repository'
import { AddCategory } from '../../usecases/add-category-usecase'
import { DbAddCategory } from '../../usecases/db-add-category-usecase'

export const makeDbAddCategory = (): AddCategory => {
  const categoryTypeORMRepository = new CategoryTypeORMRepository()
  return new DbAddCategory(categoryTypeORMRepository, categoryTypeORMRepository)
}
