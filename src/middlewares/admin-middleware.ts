import jwt from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'
import { User } from '../entities'
import { getRepository } from 'typeorm'

export const isAdmin = (
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  const token = req.headers.authorization?.split(' ')[1]
  jwt.verify(
    token as string,
    process.env.SECRET as string,
    async (err: any, decoded: any) => {
      if (err) {
        return next(err)
      }
      const userRepository = getRepository(User)
      const user = await userRepository.findOne(req.user.uuid)
      if (user) {
        const isAdm = user.role === 'admin'
        req.isAdm = isAdm
      }
      next()
    }
  )
}
