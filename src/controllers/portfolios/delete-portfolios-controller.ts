import { Request, Response } from 'express'
import { DeletePortfolioService } from '../../services/portfolios'

export class DeletePortfoliosController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const { name } = req.params
      const deletePortfoliosService = new DeletePortfolioService()
      const deletePortfolio = await deletePortfoliosService.execute(name)
      return res.status(204).json(deletePortfolio)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
