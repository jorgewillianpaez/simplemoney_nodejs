import { Request, Response } from 'express'
import { GetPortfoliosService } from '../../services/portfolios'

export class GetPortfoliosController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const getPortfoliosService = new GetPortfoliosService()
      const portfolios = await getPortfoliosService.execute()
      return res.status(200).json(portfolios)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
