import { Request, Response } from 'express'
import { UpdatePortfolioService } from '../../services/portfolios'

export class UpdatePortfoliosController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const data = req.body
      const { name } = req.params
      const updatePortfoliosService = new UpdatePortfolioService()
      const portfolioUpdated = await updatePortfoliosService.execute(
        name,
        data
      )
      return res.status(200).json(portfolioUpdated)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
