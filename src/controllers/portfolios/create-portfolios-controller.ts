import { Request, Response } from 'express'
import { CreatePortfoliosService } from '../../services/portfolios'

export class CreatePortfoliosController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const data = req.body
      const createPortfoliosService = new CreatePortfoliosService()
      const portfolio = await createPortfoliosService.execute(data)
      return res.status(201).json(portfolio)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
