import { AdminRequiredError } from '../../errors/admin-required-error'
import { NameExistsError } from '../../errors/name-exists-error'
import { badRequest, forbidden, ok, serverError } from '../../helpers'
import { Controller, HttpResponse, Validation } from '../../protocols'
import { AddCategory } from '../../usecases/add-category-usecase'

export class CreateCategoryController implements Controller {
  constructor (
    private readonly validation: Validation,
    private readonly addCategory: AddCategory
  ) {}

  async execute (request: any): Promise<HttpResponse> {
    try {
      const error = this.validation.validate(request)
      if (error) {
        return badRequest(error)
      }
      const { name, description, isAdm } = request
      if (!isAdm) return forbidden(new AdminRequiredError())
      const isValid = await this.addCategory.add({
        name,
        description
      })
      if (!isValid) return forbidden(new NameExistsError())
      return ok({
        message: `Category ${name} added to database`
      })
    } catch (error) {
      return serverError(error)
    }
  }
}
