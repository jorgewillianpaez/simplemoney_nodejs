import { AdminRequiredError } from '../../errors/admin-required-error'
import { CategoryNotFoundError } from '../../errors/category-not-found-error'
import { forbidden, noContent, notFound, serverError } from '../../helpers'
import { Controller, HttpResponse } from '../../protocols'
import { DeleteCategory } from '../../usecases/delete-category-usecase'

export class DeleteCategoryController implements Controller {
  constructor (private readonly deleteCategory: DeleteCategory) {}

  async execute (request: any): Promise<HttpResponse> {
    try {
      const { uuid, isAdm } = request
      if (!isAdm) return forbidden(new AdminRequiredError())
      const result = await this.deleteCategory.delete(uuid)
      if (!result) return notFound(new CategoryNotFoundError())
      return noContent()
    } catch (error) {
      return serverError(error)
    }
  }
}
