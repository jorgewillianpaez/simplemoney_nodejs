import { AdminRequiredError } from '../../errors/admin-required-error'
import { forbidden, noContent, ok, serverError } from '../../helpers'
import { Controller, HttpResponse } from '../../protocols'
import { LoadCategories } from '../../usecases/load-categories-usecase'

export class LoadCategoriesController implements Controller {
  constructor (private readonly loadCategories: LoadCategories) {}

  async execute (request: any): Promise<HttpResponse> {
    try {
      const { isAdm } = request
      const categories = await this.loadCategories.load(isAdm)
      if (categories === undefined) return forbidden(new AdminRequiredError())
      return categories.length ? ok(categories) : noContent()
    } catch (error) {
      return serverError(error)
    }
  }
}
