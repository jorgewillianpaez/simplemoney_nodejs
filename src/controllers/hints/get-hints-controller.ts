import { Request, Response } from 'express'
import { GetAllHints } from '../../services'

export class ListAllHintsController {
  async handle (request: Request, response: Response): Promise<any> {
    const service = new GetAllHints()
    if (!request.isAdm) {
      return response.status(401).json({ message: 'User is not Admin!' })
    }
    const hints = await service.execute()
    return response.json(hints)
  }
}
