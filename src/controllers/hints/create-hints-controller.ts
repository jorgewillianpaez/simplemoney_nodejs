import { Request, Response } from 'express'
import { CreateHintsService, validateHintData } from '../../services'

export class CreateHintController {
  async handle (request: Request, response: Response): Promise<any> {
    const hint = request.body
    const { title, body, references, type, userEmail } = request.body
    const service = new CreateHintsService()
    if (!request.isAdm) {
      return response.status(401).json({ message: 'User is not Admin!' })
    }
    const error = validateHintData().validate(hint)
    if (error) {
      return response.status(400).json({
        message: error.message
      })
    }
    const result = await service.execute({
      title,
      body,
      references,
      type,
      userEmail
    })
    if (result instanceof Error) {
      return response.status(400).json({ message: result.message })
    }
    return response.json(result)
  }
}
