import { Request, Response } from 'express'
import { DeleteHintService } from '../../services'

export class DeleteHintController {
  async handle (request: Request, response: Response): Promise<any> {
    const { id } = request.params
    const service = new DeleteHintService()
    if (!request.isAdm) {
      return response.status(401).json({ message: 'User is not Admin!' })
    }
    const result = await service.execute(id)
    if (result instanceof Error) {
      return response.status(400).json({ message: result.message })
    }
    return response.status(204).end()
  }
}
