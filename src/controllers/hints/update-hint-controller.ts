import { Request, Response } from 'express'
import { UpdateHintService } from '../../services/hints'

export class UpdateHintController {
  async handle (request: Request, response: Response): Promise<any> {
    const { id } = request.params
    const updateHint = new UpdateHintService()

    if (!request.isAdm) {
      return response.status(401).json({ message: 'User is not Admin!' })
    }

    const hint = await updateHint.execute(id, request.body)
    if (hint instanceof Error) {
      return response.status(400).json({ message: hint.message })
    }
    return response.status(200).json(hint)
  }
}
