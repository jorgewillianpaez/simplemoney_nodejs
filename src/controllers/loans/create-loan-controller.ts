import { Request, Response } from 'express'
import { CreateLoansService } from '../../services/loans'

export class CreateLoanController {
  async execute (request: Request, response: Response): Promise<any> {
    const { payments, amortizations, interests, balances } = request.body
    const service = new CreateLoansService()
    const result = await service.execute({ payments, amortizations, interests, balances })
    if (result instanceof Error) {
      return response.status(400).json({ message: result.message })
    }
    return response.json(result)
  }
}
