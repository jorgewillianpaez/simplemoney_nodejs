import { Request, Response } from 'express'
import { UpdateLoanService } from '../../services/loans'

export class UpdateLoanController {
  async execute (request: Request, response: Response): Promise<any> {
    const { id } = request.body
    const service = new UpdateLoanService()
    const result = await service.execute(id, request.body)
    if (result instanceof Error) {
      return response.status(400).json({ message: result.message })
    }
    return response.json(result)
  }
}
