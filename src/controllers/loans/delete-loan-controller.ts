
import { Request, Response } from 'express'
import { DeleteLoanService } from '../../services/loans'

export class DeleteLoanController {
  async execute (request: Request, response: Response): Promise<any> {
    const { id } = request.params
    const service = new DeleteLoanService()
    const result = await service.execute(id)
    if (result instanceof Error) {
      return response.status(400).json({ message: result.message })
    }
    return response.status(204).end()
  }
}
