import { Request, Response } from 'express'
import { GetLoansService } from '../../services/loans'

export class GetLoansController {
  async execute (request: Request, response: Response): Promise<any> {
    const service = new GetLoansService()
    const result = await service.execute()
    return response.json(result)
  }
}
