import { Request, Response } from 'express'
import { CreateExpenseService } from '../../services/expenses/create-expense-service'
import { validateExpenseData } from '../../services'

export class CreateExpenseController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const data = req.body
      const error = validateExpenseData().validate(data)
      if (error) {
        return res.status(400).json({
          message: error.message
        })
      }
      if (data.quota) {
        if (data.actual_quota > data.quota) {
          return res.status(400).json({
            message: "'Actual_quota' value cannot be greater than 'quota'"
          })
        }
      } else {
        if (data.actual_quota > 1) {
          return res.status(400).json({
            message:
              "You must set a 'quota' value greater than 'actual_quota' value"
          })
        }
      }
      const createExpenseService = new CreateExpenseService()
      const expense = await createExpenseService.execute(data)
      return res.status(201).json(expense)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
