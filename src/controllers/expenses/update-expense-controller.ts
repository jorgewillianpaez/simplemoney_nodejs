import { Request, Response } from 'express'
import { UpdateExpenseService } from '../../services/expenses/update-expense-service'

export class UpdateExpenseController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const data = req.body
      const { uuid } = req.params
      const updateExpenseService = new UpdateExpenseService()
      const expenseUpdated = await updateExpenseService.execute(
        uuid,
        data
      )
      if (!expenseUpdated) {
        return res.status(400).json({ message: "Expense doesn't exist" })
      }
      return res.status(200).json(expenseUpdated)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
