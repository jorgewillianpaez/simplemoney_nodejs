import { Request, Response } from 'express'
import { DeleteExpenseService } from '../../services/expenses/delete-expense-service'

export class DeleteExpenseController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const { uuid } = req.params
      const deleteExpenseService = new DeleteExpenseService()
      const deletedExpense = await deleteExpenseService.execute(uuid)
      if (!deletedExpense) {
        return res.status(400).json({ message: "Expense doesn't exist" })
      }
      return res.status(204).json(deletedExpense)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
