import { Request, Response } from 'express'
import { GetExpensesService } from '../../services/expenses/get-expenses-service'

export class GetExpensesController {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const getExpenseService = new GetExpensesService()
      const expenses = await getExpenseService.execute()
      return res.status(200).json(expenses)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
