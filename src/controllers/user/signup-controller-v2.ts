import { EmailExistsError } from '../../errors'
import { badRequest, forbidden, ok, serverError } from '../../helpers'
import { Controller, HttpResponse, Validation } from '../../protocols'
import { AddUser } from '../../usecases/add-user-usecase'

export class SignUpController implements Controller {
  constructor (
    private readonly validation: Validation,
    private readonly addUser: AddUser
  ) {}

  async execute (request: SignUpController.Request): Promise<HttpResponse> {
    try {
      const error = this.validation.validate(request)
      if (error) {
        return badRequest(error)
      }
      const { name, cpf, email, password, phone, role, subscription, imageUrl } = request
      const isValid = await this.addUser.add({
        name,
        cpf,
        email,
        password,
        phone,
        role,
        subscription,
        imageUrl
      })
      if (!isValid) return forbidden(new EmailExistsError())
      return ok({
        name,
        cpf,
        email,
        phone,
        role,
        subscription,
        imageUrl
      })
    } catch (error) {
      return serverError(error)
    }
  }
}

export namespace SignUpController {
  export type Request = {
    name: string
    cpf: string
    email: string
    password: string
    phone: string
    role: string
    subscription: string
    imageUrl: string
  }
}
