import { Request, Response } from 'express'
import { authenticateUser } from '../../services'

export class SignIn {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const { email, password } = req.body
      const token = await authenticateUser(email, password)
      if (!token) {
        return res.status(400).json({ message: 'Wrong email/password' })
      }
      return res.status(200).json({ token: token })
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
