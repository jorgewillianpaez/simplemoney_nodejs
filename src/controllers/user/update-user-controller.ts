import { Request, Response } from 'express'
import { UpdateUserService } from '../../services'

export class UpdateUserController {
  async update (req: Request, res: Response): Promise<any> {
    try {
      const { uuid } = req.params
      const updateUserService = new UpdateUserService()
      const updatedUser = await updateUserService.updateUser(uuid, req.body)
      if (!updatedUser) {
        return res.status(404).json({
          message: 'User not found'
        })
      }
      return res.status(200).json(updatedUser)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
