import { Request, Response } from 'express'
import { resetPasswordTokenService , transport } from '../../services'

export class ResetPasswordTokenController {
  async execute (req: Request, res: Response): Promise<Response> {
    try {
      const { email } = req.body
      const token = await resetPasswordTokenService(email)
      if (!token) {
        return res.status(401).json({
          message: 'E-mail not found'
        })
      }
      const mailOptions = {
        from: process.env.MAILTRAP_FROM,
        to: process.env.MAILTRAP_TO,
        subject: 'Restauração de senha',
        text: `Código para restauração de senha: ${token}`
      }
      transport.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error)
        } else {
          console.log(info)
        }
      })
      return res.status(200).json({ message: token })
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
