import { Request, Response } from 'express'
import { GetUsersService } from '../../services/user/get-users-service'

export class GetUsersController {
  async handle (request: Request, response: Response): Promise<any> {
    const service = new GetUsersService()
    if (!request.isAdm) {
      return response.status(401).json({ message: 'User is not Admin!' })
    }
    const users = await service.execute()
    return response.json(users)
  }
}
