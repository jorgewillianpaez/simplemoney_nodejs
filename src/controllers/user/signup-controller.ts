import { Request, Response } from 'express'
import { checkIfUserExists, signUpUser, validateSignUpData } from '../../services'

export class SignUp {
  async execute (req: Request, res: Response): Promise<any> {
    try {
      const data = req.body
      let error = validateSignUpData().validate(data)
      error = await checkIfUserExists(data)
      if (error) {
        return res.status(400).json({
          message: error.message
        })
      }
      const user = await signUpUser(data)
      return res.status(201).json(user)
    } catch (error: any) {
      return res.status(500).json({
        message: error.message
      })
    }
  }
}
