import { Request, Response } from 'express'
import { UpdateAccountService } from '../../services/accounts/update-accounts-service'

export class UpdateAccountsController {
  async handle (request: Request, response: Response): Promise<any> {
    const { id } = request.params
    const updateAccount = new UpdateAccountService()

    const account = await updateAccount.execute(id, request.body)
    if (account instanceof Error) {
      return response.status(400).json({ message: account.message })
    }
    return response.status(200).json(account)
  }
}
