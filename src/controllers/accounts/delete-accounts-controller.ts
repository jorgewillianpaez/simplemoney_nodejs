import { Request, Response } from 'express'
import { DeleteAccountsService } from '../../services/accounts/delete-accounts-service'

export class DeleteAccountsController {
  async handle (request: Request, response: Response): Promise<any> {
    const { id } = request.params
    const service = new DeleteAccountsService()

    const result = await service.execute(id)
    if (result instanceof Error) {
      return response.status(400).json({ message: result.message })
    }
    return response.status(204).end()
  }
}
