import { Request, Response } from 'express'
import { GetAccountsService } from '../../services/accounts/get-accounts-service'

export class GetAccountsController {
  async handle (request: Request, response: Response): Promise<any> {
    const service = new GetAccountsService()

    const accounts = await service.execute()
    return response.json(accounts)
  }
}
