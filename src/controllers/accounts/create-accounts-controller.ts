import { Request, Response } from 'express'
import { CreateAccountsService, validateAccountData } from '../../services/accounts/create-accounts-service'

export class CreateAccountsController {
  async handle (request: Request, response: Response): Promise<any> {
    const account = request.body
    const { name, balance, userEmail } = request.body
    const service = new CreateAccountsService()

    const error = validateAccountData().validate(account)
    if (error) {
      return response.status(400).json({
        message: error.message
      })
    }
    const result = await service.execute({
      name,
      balance,
      userEmail
    })
    if (result instanceof Error) {
      return response.status(400).json({ message: result.message })
    }
    return response.json(result)
  }
}
