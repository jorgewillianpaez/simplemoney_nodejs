import { MissingParamError } from '../errors'
import { Validation } from '../protocols'

export class RequiredFieldValidation implements Validation {
  constructor (private readonly field: string) {}
  validate (input: any): Error | undefined {
    if (!input[this.field]) {
      return new MissingParamError(this.field)
    }
    return undefined
  }
}
