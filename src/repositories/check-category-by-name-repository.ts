export interface CheckCategoryByNameRepository {
  checkByName: (name: string) => Promise<CheckCategoryByNameRepository.Result>
}

export namespace CheckCategoryByNameRepository {
  export type Result = boolean
}
