import { EntityRepository, Repository } from 'typeorm'
import { Accounts } from '../entities'

@EntityRepository(Accounts)
export class AccountsRepository extends Repository<Accounts> {
  public async findByName (name: string): Promise<Accounts | undefined> {
    try {
      const accounts = await this.findOne({
        where: {
          name
        }
      })
      return accounts
    } catch (error) {
      return undefined
    }
  }
}
