import { EntityRepository, Repository } from 'typeorm'
import { Portfolio } from '../entities'

@EntityRepository(Portfolio)
export class PortfoliosRepository extends Repository<Portfolio> {
  public async findByName (name: string): Promise<Portfolio | undefined> {
    try {
      const portfolio = await this.findOne({
        where: {
          name
        }
      })
      return portfolio
    } catch (error) {
      return undefined
    }
  }
}
