import { getCustomRepository, getRepository } from 'typeorm'
import { User } from '../entities'
import { AddUser } from '../usecases/add-user-usecase'
import { AddUserRepository } from './add-user-repository'
import { CheckUserByEmailRepository } from './check-account-by-email-repository'
import { UserRepository } from './userRepository'

interface InsertData {
  name: string
  cpf: string
  email: string
  password: string
  phone: string
  role: string
  subscription: string
  imageUrl?: string
  image_url: string
}

export class UserTypeORMRepository implements CheckUserByEmailRepository, AddUserRepository {
  async add (data: AddUserRepository.Params): Promise<AddUser.Result> {
    const userRepository = getRepository(User)
    const insertData: InsertData = { ...data, image_url: data.imageUrl }
    delete insertData.imageUrl
    const user = userRepository.create(insertData)
    await userRepository.save(user)
    return user.id !== null && user.id !== undefined
  }

  async checkByEmail (email: string): Promise<CheckUserByEmailRepository.Result> {
    const userRepository = getCustomRepository(UserRepository)
    const user = await userRepository.findByEmail(email)
    console.log(user)
    if (!user) {
      return false
    }
    return true
  }
}
