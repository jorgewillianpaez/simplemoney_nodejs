import { EntityRepository, Repository } from 'typeorm'
import { Category } from '../entities'

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  public async findByName (name: string): Promise<Category | undefined> {
    try {
      const category = await this.findOne({
        where: {
          name
        }
      })
      return category
    } catch (error) {
      return undefined
    }
  }
}
