import { getCustomRepository, getRepository } from 'typeorm'
import { Category } from '../entities'
import { AddCategory } from '../usecases/add-category-usecase'
import { AddCategoryRepository } from './add-category-repository'
import { CategoryRepository } from './categoryRepository'
import { CheckCategoryByNameRepository } from './check-category-by-name-repository'
import { DeleteCategoryRepository } from './delete-category-repository'
import { LoadCategoriesRepository } from './load-categories-repository'

export class CategoryTypeORMRepository implements CheckCategoryByNameRepository, AddCategory, LoadCategoriesRepository, DeleteCategoryRepository {
  async add (categoryData: AddCategoryRepository.Params): Promise<AddCategory.Result> {
    const categoryRepository = getRepository(Category)
    const category = categoryRepository.create(categoryData)
    await categoryRepository.save(category)
    return category.id !== null && category.id !== undefined
  }

  async checkByName (name: string): Promise<CheckCategoryByNameRepository.Result> {
    const categoryRepository = getCustomRepository(CategoryRepository)
    const category = await categoryRepository.findByName(name)
    if (!category) {
      return false
    }
    return true
  }

  async loadAll (isAdm: boolean): Promise<LoadCategoriesRepository.Result> {
    const categoryRepository = getRepository(Category)
    if (!isAdm) return undefined
    return await categoryRepository.find()
  }

  async delete (categoryId: string): Promise<boolean> {
    try {
      const categoryRepository = getRepository(Category)
      const category = await categoryRepository.findOne({
        where: {
          id: categoryId
        }
      })
      if (!category) return false
      await categoryRepository.delete(category)
      return true
    } catch (error) {
      return false
    }
  }
}
