import { EntityRepository, Repository } from 'typeorm'
import { User } from '../entities'

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  public async findByEmail (email: string): Promise<User | undefined> {
    try {
      const user = await this.findOne({
        where: {
          email
        }
      })
      return user
    } catch (error) {
      return undefined
    }
  }

  public async findByCPF (cpf: string): Promise<User | undefined> {
    try {
      const user = await this.findOne({
        where: {
          cpf
        }
      })
      return user
    } catch (error) {
      return undefined
    }
  }

  public async findByPhone (phone: string): Promise<User | undefined> {
    try {
      const user = await this.findOne({
        where: {
          phone
        }
      })
      return user
    } catch (error) {
      return undefined
    }
  }
}
