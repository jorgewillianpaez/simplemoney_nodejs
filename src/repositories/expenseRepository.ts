import { EntityRepository, Repository } from 'typeorm'
import { Expenses } from '../entities'

@EntityRepository(Expenses)
export class ExpenseRepository extends Repository<Expenses> {
  public async findByName (name: string): Promise<Expenses | undefined> {
    try {
      const expenses = await this.findOne({
        where: {
          name
        }
      })
      return expenses
    } catch (error) {
      return undefined
    }
  }
}
