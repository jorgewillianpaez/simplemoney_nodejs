import { Category } from '../entities'

export interface LoadCategoriesRepository {
  loadAll: (isAdm: boolean) => Promise<LoadCategoriesRepository.Result>
}

export namespace LoadCategoriesRepository {
  export type Result = Category[] | undefined
}
