import { Router } from 'express'

import { ResetPasswordController } from '../controllers/user/reset-password-controller'

const router = Router()

export const resetPasswordRouter = (): Router => {
  const resetPasswordController = new ResetPasswordController()
  router.post('', resetPasswordController.execute)
  return router
}
