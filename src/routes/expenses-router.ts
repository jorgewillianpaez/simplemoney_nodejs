import { Router } from 'express'

import { CreateExpenseController } from '../controllers/expenses/create-expense-controller'
import { DeleteExpenseController } from '../controllers/expenses/delete-expense-controller'
import { UpdateExpenseController } from '../controllers/expenses/update-expense-controller'
import { GetExpensesController } from '../controllers/expenses/get-expenses-controller'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const expensesRouter = (): Router => {
  router.post('', isAuthenticated, new CreateExpenseController().execute)
  router.get('/list', isAuthenticated, new GetExpensesController().execute)
  router.patch(
    '/:uuid',
    isAuthenticated,
    new UpdateExpenseController().execute
  )
  router.delete(
    '/delete/:uuid',
    isAuthenticated,
    new DeleteExpenseController().execute
  )

  return router
}
