import { Router } from 'express'
import { UpdateUserController } from '../controllers/user/update-user-controller'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const updateUserRouter = (): Router => {
  const updateUserController = new UpdateUserController()
  router.put('/:id', isAuthenticated, updateUserController.update)

  return router
}
