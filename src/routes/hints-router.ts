import { Router } from 'express'

import { CreateHintController } from '../controllers/hints/create-hints-controller'
import { DeleteHintController } from '../controllers/hints/delete-hint-controller'
import { ListAllHintsController } from '../controllers/hints/get-hints-controller'
import { UpdateHintController } from '../controllers/hints/update-hint-controller'
import { isAdmin } from '../middlewares/admin-middleware'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const hintsRouter = (): Router => {
  router.post('', isAuthenticated, isAdmin ,new CreateHintController().handle)
  router.get('/list', isAuthenticated, isAdmin, new ListAllHintsController().handle)
  router.delete('/delete/:id', isAuthenticated, isAdmin, new DeleteHintController().handle)
  router.put('/:id', isAuthenticated, isAdmin, new UpdateHintController().handle)

  return router
}
