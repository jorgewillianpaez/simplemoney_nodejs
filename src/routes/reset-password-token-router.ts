import { Router } from 'express'

import { ResetPasswordTokenController } from '../controllers/user/reset-password-token-controller'

const router = Router()

export const resetPasswordTokenRouter = (): Router => {
  const resetPasswordTokenController = new ResetPasswordTokenController()
  router.post('', resetPasswordTokenController.execute)
  return router
}
