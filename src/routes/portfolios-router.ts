import { Router } from 'express'

import { CreatePortfoliosController } from '../controllers/portfolios/create-portfolios-controller'
import { DeletePortfoliosController } from '../controllers/portfolios/delete-portfolios-controller'
import { UpdatePortfoliosController } from '../controllers/portfolios/update-portfolios-controller'
import { GetPortfoliosController } from '../controllers/portfolios/get-portfolios-controller'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const portfoliosRouter = (): Router => {
  router.post(
    '',
    isAuthenticated,
    new CreatePortfoliosController().execute
  )
  router.get(
    '/list',
    isAuthenticated,
    new GetPortfoliosController().execute
  )
  router.patch(
    '/:uuid',
    isAuthenticated,
    new UpdatePortfoliosController().execute
  )
  router.delete(
    '/delete/:uuid',
    isAuthenticated,
    new DeletePortfoliosController().execute
  )
  return router
}
