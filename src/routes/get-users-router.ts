import { Router } from 'express'
import { GetUsersController } from '../controllers/user/get-users-controller'
import { isAdmin } from '../middlewares/admin-middleware'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const getUsersRouter = (): Router => {
  const getUsersController = new GetUsersController()
  router.get('/', isAuthenticated, isAdmin, getUsersController.handle)

  return router
}
