import { Router } from 'express'

import { SignIn } from '../controllers/user/signin-controller'

const router = Router()

export const signInRouter = (): Router => {
  const signIn = new SignIn()
  router.post('', signIn.execute)
  return router
}
