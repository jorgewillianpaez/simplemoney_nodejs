import { Router } from 'express'
import {
  CreateLoanController,
  DeleteLoanController,
  GetLoansController,
  UpdateLoanController
} from '../controllers/loans'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const loansRouter = (): Router => {
  router.post('', isAuthenticated, new CreateLoanController().execute)
  router.get('/list', isAuthenticated, new GetLoansController().execute)
  router.delete('/delete/:id', isAuthenticated, new DeleteLoanController().execute)
  router.patch('/:id', isAuthenticated, new UpdateLoanController().execute)

  return router
}
