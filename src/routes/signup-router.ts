import { Router } from 'express'
import { routeAdapter } from '../adapters/express-route-adapter'
import { SignUp } from '../controllers/user/signup-controller'
import { makeSignUpController } from '../factories/controllers/signup-controller-factory'

const router = Router()

export const signUpRouter = (): Router => {
  const signUp = new SignUp()
  router.post('', signUp.execute)
  router.post('/v2', routeAdapter(makeSignUpController()))
  return router
}
