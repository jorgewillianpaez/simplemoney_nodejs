import { Express } from 'express'
import { signInRouter } from './signin-router'
import { signUpRouter } from './signup-router'
import { updateUserRouter } from './update-user-router'
import { hintsRouter } from './hints-router'
import { resetPasswordTokenRouter } from './reset-password-token-router'
import { resetPasswordRouter } from './reset-password-router'
import { expensesRouter } from './expenses-router'
import { accountsRouter } from './accounts-router'
import { loansRouter } from './loans-router'
import { portfoliosRouter } from './portfolios-router'
import { categoryRouter } from './category-router'
import swaggerUiExpress from 'swagger-ui-express'
import swaggerDocs from '../swagger.json'
import { getUsersRouter } from './get-users-router'

export const initializerRouter = (app: Express): void => {
  app.use('/api/login', signInRouter())
  app.use('/api/register', signUpRouter())
  app.use('/api/user', updateUserRouter())
  app.use('/api/users', getUsersRouter())
  app.use('/api/hints', hintsRouter())
  app.use('/api/reset-password-token', resetPasswordTokenRouter())
  app.use('/api/reset-password', resetPasswordRouter())
  app.use('/api/expenses', expensesRouter())
  app.use('/api/accounts', accountsRouter())
  app.use('/api/loans', loansRouter())
  app.use('/api/portfolios', portfoliosRouter())
  app.use('/api/category', categoryRouter())
  app.use('/api-docs', swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocs))
}
