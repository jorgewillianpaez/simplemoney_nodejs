import { Router } from 'express'
import { CreateAccountsController } from '../controllers/accounts/create-accounts-controller'
import { DeleteAccountsController } from '../controllers/accounts/delete-accounts-controller'
import { GetAccountsController } from '../controllers/accounts/get-accounts-controller'
import { UpdateAccountsController } from '../controllers/accounts/update-accounts-controller'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const accountsRouter = (): Router => {
  router.post('', isAuthenticated, new CreateAccountsController().handle)
  router.get('/list', isAuthenticated, new GetAccountsController().handle)
  router.patch('/:id', isAuthenticated, new UpdateAccountsController().handle)
  router.delete('/delete/:id', isAuthenticated, new DeleteAccountsController().handle)
  return router
}
