import { Router } from 'express'
import { routeAdapter } from '../adapters/express-route-adapter'
import { makeCreateCategoryController } from '../factories/controllers/create-category-controller-factory'
import { makeDeleteCategoryController } from '../factories/controllers/delete-category-controller-factory'
import { makeLoadCategoriesController } from '../factories/controllers/load-categories-controller-factory'
import { isAdmin } from '../middlewares/admin-middleware'
import { isAuthenticated } from '../middlewares/authentication-middleware'

const router = Router()

export const categoryRouter = (): Router => {
  router.post(
    '/add',
    isAuthenticated,
    isAdmin,
    routeAdapter(makeCreateCategoryController())
  )
  router.get(
    '/list',
    isAuthenticated,
    isAdmin,
    routeAdapter(makeLoadCategoriesController())
  )
  router.delete(
    '/:uuid',
    isAuthenticated,
    isAdmin,
    routeAdapter(makeDeleteCategoryController())
  )
  return router
}
