export class AdminRequiredError extends Error {
  constructor () {
    super('You must be an Admin to access this data')
    this.name = 'AdminRequiredError'
  }
}
