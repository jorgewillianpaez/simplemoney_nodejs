export class EmailExistsError extends Error {
  constructor () {
    super('The received email already exists')
    this.name = 'EmailExistsError'
  }
}
