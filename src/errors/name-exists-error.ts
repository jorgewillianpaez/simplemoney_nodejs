export class NameExistsError extends Error {
  constructor () {
    super('The received name already exists')
    this.name = 'NameExistsError'
  }
}
