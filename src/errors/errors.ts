export class ErrorHandler extends Error {
  statusCode: any
  constructor (statusCode: any, message: any) {
    super()
    this.message = message
    this.statusCode = statusCode
  }
}

export const handleError = async (err: any, res: any): Promise<any> => {
  const { statusCode, message } = err
  return res.status(statusCode).json({
    status: 'error',
    statusCode,
    message
  })
}
