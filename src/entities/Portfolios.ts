import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('portfolios')
export class Portfolio {
  @PrimaryGeneratedColumn()
  name: string

  @Column()
  profileType: string

  @Column()
  stocks: string

  @Column()
  objective: string
}
