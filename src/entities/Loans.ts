import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('loans')
export class Loans {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  payments: string

  @Column()
  amortizations: string

  @Column()
  interests: string

  @Column()
  balances: string
}
