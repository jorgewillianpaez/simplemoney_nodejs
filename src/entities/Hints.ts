import { Column, PrimaryGeneratedColumn, Entity, ManyToOne } from 'typeorm'
import { User } from './User'

@Entity('hints')
export class Hints {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ nullable: false })
  title: string

  @Column({ nullable: false })
  body: string

  @Column()
  references: string

  @Column({ nullable: false })
  type: string

  @Column({ nullable: false })
  user_email: string

  @ManyToOne(() => User, (user) => user.hints)
  user: User
}
