import {
  Column,
  Entity,
  BeforeInsert,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany
} from 'typeorm'
import bcrypt from 'bcrypt'
import { Hints } from './Hints'
import { Accounts } from './Accounts'

@Entity('user')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ nullable: false })
  name: string

  @Column({ nullable: true })
  cpf: string

  @Column({ unique: true, nullable: false })
  email: string

  @Column({ nullable: false })
  password: string

  @Column({ unique: true, nullable: true })
  phone: string

  @Column()
  role: string

  @Column()
  subscription: string

  @Column()
  image_url: string

  @CreateDateColumn()
  created_at: Date

  @OneToMany(() => Hints, (hints) => hints.user) hints: Hints[]

  @OneToMany(() => Accounts, (accounts) => accounts.user)
  accounts!: Accounts[]

  @BeforeInsert()
  hashPassword (): void {
    this.password = bcrypt.hashSync(this.password, 10)
  }
}
