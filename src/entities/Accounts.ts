import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne
} from 'typeorm'
import { Expenses } from '.'
import { User } from './User'

@Entity('accounts')
export class Accounts {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  name: string

  @Column()
  balance: number

  @Column()
  user_email: string

  @ManyToOne(() => User, (user) => user.accounts)
  user!: User

  @OneToMany(() => Expenses, (expense) => expense.account)
  expenses: Expenses[]
}
