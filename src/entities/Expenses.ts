import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm'
import { Accounts } from './Accounts'

@Entity('expenses')
export class Expenses {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ nullable: false })
  name: string

  @Column({ nullable: true, type: 'float' })
  value: number

  @Column({ default: '1' })
  quota: number

  @Column()
  actual_quota: number

  @Column({ nullable: true })
  type: string

  @Column({ nullable: true })
  category: string

  @ManyToOne((type) => Accounts, (account) => account.expenses)
  account: Accounts
}
