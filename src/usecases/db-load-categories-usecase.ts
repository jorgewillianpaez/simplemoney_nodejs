import { LoadCategoriesRepository } from '../repositories/load-categories-repository'
import { LoadCategories } from './load-categories-usecase'

export class DbLoadCategories {
  constructor (private readonly loadCategoriesRepository: LoadCategoriesRepository) {}

  async load (isAdm: boolean): Promise<LoadCategories.Result> {
    return this.loadCategoriesRepository.loadAll(isAdm)
  }
}
