export interface AddUser {
  add: (user: AddUser.Params) => Promise<AddUser.Result>
}

export namespace AddUser {
  export type Params = {
    name: string
    cpf: string
    email: string
    password: string
    phone: string
    role: string
    subscription: string
    imageUrl: string
  }
  export type Result = boolean
}
