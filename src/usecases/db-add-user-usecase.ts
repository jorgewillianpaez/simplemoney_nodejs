import { AddUserRepository } from '../repositories/add-user-repository'
import { CheckUserByEmailRepository } from '../repositories/check-account-by-email-repository'
import { AddUser } from './add-user-usecase'

export class DbAddUser implements AddUser {
  constructor (
    private readonly checkUserByEmailRepository: CheckUserByEmailRepository,
    private readonly addUserRepository: AddUserRepository
  ) {}

  async add (userData: AddUser.Params): Promise<AddUser.Result> {
    const exists = await this.checkUserByEmailRepository.checkByEmail(userData.email)
    let isValid = false
    if (!exists) {
      isValid = await this.addUserRepository.add({ ...userData })
    }
    return isValid
  }
}
