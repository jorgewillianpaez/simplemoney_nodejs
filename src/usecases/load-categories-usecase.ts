import { Category } from '../entities'

export interface LoadCategories {
  load: (isAdm: boolean) => Promise<LoadCategories.Result>
}

export namespace LoadCategories {
  export type Result = Category[] | undefined
}
