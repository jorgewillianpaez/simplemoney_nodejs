import { DeleteCategoryRepository } from '../repositories/delete-category-repository'
import { DeleteCategory } from './delete-category-usecase'

export class DbDeleteCategory {
  constructor (private readonly deleteCategoryRepository: DeleteCategoryRepository) {}

  async delete (userId: string): Promise<DeleteCategory.Result> {
    return this.deleteCategoryRepository.delete(userId)
  }
}
