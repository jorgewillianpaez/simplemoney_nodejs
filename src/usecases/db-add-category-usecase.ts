import { AddCategoryRepository } from '../repositories/add-category-repository'
import { CheckCategoryByNameRepository } from '../repositories/check-category-by-name-repository'
import { AddCategory } from './add-category-usecase'

export class DbAddCategory implements AddCategory {
  constructor (
    private readonly addCategoryRepository: AddCategoryRepository,
    private readonly checkCategoryByNameRepository: CheckCategoryByNameRepository
  ) {}

  async add (category: AddCategory.Params): Promise<AddCategory.Result> {
    const exists = await this.checkCategoryByNameRepository.checkByName(category.name)
    let isValid = false
    if (!exists) {
      isValid = await this.addCategoryRepository.add({ ...category })
    }
    return isValid
  }
}
