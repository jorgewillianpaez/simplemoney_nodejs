import { getCustomRepository, getRepository } from 'typeorm'
import { Accounts } from '../../entities'
import { Validation } from '../../protocols'
import { RequiredFieldValidation , ValidationComposite } from '../../validation'
import { ErrorHandler } from '../../errors/errors'
import { UserRepository } from '../../repositories/userRepository'

interface AccountsProps {
  name: string
  balance: number
  userEmail: string
}

export class CreateAccountsService {
  async execute ({ name, balance, userEmail }: AccountsProps): Promise<Accounts | Error> {
    const accountsRepository = getRepository(Accounts)
    const userRepository = getCustomRepository(UserRepository)
    const user = await userRepository.findByEmail(userEmail)
    if (!user) {
      return new ErrorHandler(400, 'User does not exists!')
    }
    if (await accountsRepository.findOne({ name })) {
      return new ErrorHandler(400, 'Account already exists!')
    }
    const accounts = accountsRepository.create({
      name,
      balance,
      user_email: userEmail,
      user
    })
    await accountsRepository.save(accounts)
    return accounts
  }
}

export const validateAccountData = (): Validation => {
  const validationsField: Validation[] = []

  for (const field of ['name', 'balance', 'userEmail']) {
    validationsField.push(new RequiredFieldValidation(field))
  }

  return new ValidationComposite(validationsField)
}
