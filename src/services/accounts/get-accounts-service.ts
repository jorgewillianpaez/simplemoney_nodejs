import { getRepository } from 'typeorm'
import { Accounts } from '../../entities'

export class GetAccountsService {
  async execute (): Promise<Accounts[]> {
    const accountsRepository = getRepository(Accounts)
    const accounts = await accountsRepository.find({
      relations: ['user']
    })
    return accounts
  }
}
