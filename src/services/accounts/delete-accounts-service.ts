import { getRepository } from 'typeorm'
import { Accounts } from '../../entities'
import { ErrorHandler } from '../../errors/errors'

export class DeleteAccountsService {
  async execute (id: string): Promise<any> {
    const repo = getRepository(Accounts)
    if (!(await repo.findOne(id))) {
      return new ErrorHandler(400, 'Account does not exists!')
    }
    await repo.delete(id)
  }
}
