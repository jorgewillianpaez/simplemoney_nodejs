import { getRepository } from 'typeorm'
import { Accounts } from '../../entities'
import { ErrorHandler } from '../../errors/errors'

export class UpdateAccountService {
  async execute (id: string, data: any): Promise<Accounts | Error> {
    const userRepository = getRepository(Accounts)
    const account = await userRepository.findOne(id)
    if (!account) {
      return new ErrorHandler(400, 'Account does not exists!')
    }

    if (data.userEmail) {
      return new ErrorHandler(400, 'Invalid field!')
    }

    return await userRepository.save({
      ...account,
      ...data
    })
  }
}
