import { getCustomRepository } from 'typeorm'
import { PortfoliosRepository } from '../../repositories/portfolios-repository'
import { Portfolio } from '../../entities'

export class CreatePortfoliosService {
  async execute (data: {}): Promise<Portfolio | undefined> {
    try {
      const portfoliosRepository = getCustomRepository(PortfoliosRepository)
      const portfolio = portfoliosRepository.create(data)
      await portfoliosRepository.save(portfolio)
      return portfolio
    } catch (error) {
      return undefined
    }
  }
}
