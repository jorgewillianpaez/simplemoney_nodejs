import { getCustomRepository } from 'typeorm'
import { PortfoliosRepository } from '../../repositories/portfolios-repository'

export class DeletePortfolioService {
  async execute (uuid: string): Promise<any | undefined> {
    try {
      const portfoliosRepository = getCustomRepository(PortfoliosRepository)
      await portfoliosRepository.delete(uuid)
      return 'Portfolio deleted!'
    } catch (error) {
      return undefined
    }
  }
}
