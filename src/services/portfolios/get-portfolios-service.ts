import { getCustomRepository } from 'typeorm'
import { Portfolio } from '../../entities'
import { PortfoliosRepository } from '../../repositories/portfolios-repository'

export class GetPortfoliosService {
  async execute (): Promise<Portfolio[] | undefined> {
    try {
      const portfoliosRepository = getCustomRepository(PortfoliosRepository)
      const portfolios = await portfoliosRepository.find()
      return portfolios
    } catch (error) {
      return undefined
    }
  }
}
