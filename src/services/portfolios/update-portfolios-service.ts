import { PortfoliosRepository } from '../../repositories/portfolios-repository'
import { getCustomRepository } from 'typeorm'
import { Portfolio } from '../../entities'

export class UpdatePortfolioService {
  async execute (uuid: string, data: {}): Promise<Portfolio | undefined> {
    try {
      const portfoliosRepository = getCustomRepository(PortfoliosRepository)
      const portfolio = await portfoliosRepository.findOne(uuid)
      const portfolioUpdated = await portfoliosRepository.save({
        ...portfolio,
        ...data
      })
      return portfolioUpdated
    } catch (error) {
      return undefined
    }
  }
}
