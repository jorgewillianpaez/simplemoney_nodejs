import { getRepository } from 'typeorm'
import { User } from '../../entities'

export class UpdateUserService {
  async updateUser (uuid: string, data: any): Promise<User | undefined> {
    try {
      const userRepository = getRepository(User)
      const user = await userRepository.findOne(uuid)
      return await userRepository.save({
        ...user,
        ...data
      })
    } catch (error) {
      return undefined
    }
  }
}
