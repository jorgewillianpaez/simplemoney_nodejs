import jwt from 'jsonwebtoken'
import { getCustomRepository } from 'typeorm'
import { UserRepository } from '../../repositories/userRepository'

export const resetPasswordTokenService = async (
  email: string
): Promise<any | undefined> => {
  const userRepository = getCustomRepository(UserRepository)
  const user = await userRepository.findOne({ email })
  if (!user) {
    return undefined
  }
  const token = jwt.sign({ uuid: user.id }, process.env.SECRET as string, {
    expiresIn: '10m'
  })
  return token
}
