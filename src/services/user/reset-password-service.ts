import jwt from 'jsonwebtoken'
import * as bcrypt from 'bcrypt'
import { getCustomRepository } from 'typeorm'
import { UserRepository } from '../../repositories/userRepository'

export const resetPasswordService = async (
  token: string,
  newPassword: string
): Promise<any> => {
  const userRepository = getCustomRepository(UserRepository)
  const result = jwt.verify(
    token,
    process.env.SECRET as string,
    async (err: any, decoded: any) => {
      if (err) {
        return undefined
      }
      const hashedPassword = bcrypt.hashSync(newPassword, 10)
      const data = {
        password: hashedPassword
      }
      await userRepository.update(decoded.uuid, data)
      return 'Password reseted'
    }
  )

  return result
}
