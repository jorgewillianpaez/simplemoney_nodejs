import { UserRepository } from '../../repositories/userRepository'
import { getCustomRepository } from 'typeorm'
import { RequiredFieldValidation, ValidationComposite } from '../../validation'
import { Validation } from '../../protocols'

interface UserData {
  id: string
  name: string
  cpf: string
  email: string
  password?: string
  phone: string
  role: string
  subscription: string
  image_url: string
  created_at: Date
}

export const signUpUser = async (data: {}): Promise<UserData> => {
  const userRepository = getCustomRepository(UserRepository)
  const user = userRepository.create(data)
  await userRepository.save(user)
  const userData: UserData = { ...user }
  delete userData.password
  return userData
}

export const validateSignUpData = (): Validation => {
  const validations: Validation[] = []
  for (const field of ['name', 'cpf', 'email', 'password', 'phone', 'role', 'subscription', 'image_url']) {
    validations.push(new RequiredFieldValidation(field))
  }
  return new ValidationComposite(validations)
}

export const checkIfUserExists = async (data: any): Promise<Error | undefined> => {
  try {
    const userRepository = getCustomRepository(UserRepository)
    let user = await userRepository.findByEmail(data.email)
    if (user?.id) {
      return Error('The given email already exists')
    }
    user = await userRepository.findByCPF(data.cpf)
    if (user?.id) {
      return Error('The given cpf already exists')
    }
    user = await userRepository.findByPhone(data.phone)
    if (user?.id) {
      return Error('The given phone already exists')
    }
  } catch (error) {
    return undefined
  }
}
