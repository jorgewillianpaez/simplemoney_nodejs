import nodemailer from 'nodemailer'
import * as dotenv from 'dotenv'

dotenv.config()

export const transport = nodemailer.createTransport({
  host: 'smtp.mailtrap.io',
  port: 2525,
  auth: {
    user: process.env.MAILTRAP_USER_HASH,
    pass: process.env.MAILTRAP_PASS
  }
})
