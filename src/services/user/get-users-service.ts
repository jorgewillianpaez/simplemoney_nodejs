import { getRepository } from 'typeorm'
import { User } from '../../entities'

export class GetUsersService {
  async execute (): Promise<User[]> {
    const usersRepository = getRepository(User)
    const users = await usersRepository.find()
    return users
  }
}
