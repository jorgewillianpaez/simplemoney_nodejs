import { getRepository } from 'typeorm'
import { Loans } from '../../entities'
import { ErrorHandler } from '../../errors/errors'

export class UpdateLoanService {
  async execute (id: string, data: any): Promise<Loans | Error> {
    const repo = getRepository(Loans)
    const loan = await repo.findOne(id)
    if (!loan) {
      return new ErrorHandler(400, 'Hint does not exists!')
    }
    return await repo.save({
      ...loan,
      ...data
    })
  }
}
