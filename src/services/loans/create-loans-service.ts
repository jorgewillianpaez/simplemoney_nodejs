import { getRepository } from 'typeorm'
import { Loans } from '../../entities'
import { ErrorHandler } from '../../errors/errors'

interface LoansProp {
  payments: string
  amortizations: string
  interests: string
  balances: string

}

export class CreateLoansService {
  async execute ({ payments, amortizations, interests, balances }: LoansProp): Promise<Loans | Error> {
    const repo = getRepository(Loans)
    if (await repo.findOne({ payments })) {
      return new ErrorHandler(400, 'Loan already exists!')
    }
    const loan = repo.create({
      payments,
      amortizations,
      interests,
      balances
    })
    await repo.save(loan)
    return loan
  }
}
