import { getRepository } from 'typeorm'
import { Loans } from '../../entities'
import { ErrorHandler } from '../../errors/errors'

export class DeleteLoanService {
  async execute (id: string): Promise<any> {
    const repo = getRepository(Loans)
    if (!(await repo.findOne(id))) {
      return new ErrorHandler(400, 'Loan does not exists!')
    }
    await repo.delete(id)
    return 'Loan deleted!'
  }
}
