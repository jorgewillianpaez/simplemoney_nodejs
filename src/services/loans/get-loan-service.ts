import { getRepository } from 'typeorm'
import { Loans } from '../../entities'

export class GetLoansService {
  async execute (): Promise<any> {
    const repo = getRepository(Loans)
    const hints = await repo.find()
    return hints
  }
}
