import { getRepository } from 'typeorm'
import { Hints } from '../../entities/Hints'

export class GetAllHints {
  async execute (): Promise<Hints[]> {
    const repo = getRepository(Hints)
    const hints = await repo.find({
      relations: ['user']
    })
    return hints
  }
}
