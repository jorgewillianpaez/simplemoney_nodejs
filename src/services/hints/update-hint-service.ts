import { getRepository } from 'typeorm'
import { Hints } from '../../entities'
import { ErrorHandler } from '../../errors/errors'

export class UpdateHintService {
  async execute (id: string, data: any): Promise<Hints | Error> {
    const userRepository = getRepository(Hints)
    const hint = await userRepository.findOne(id)
    if (!hint) {
      return new ErrorHandler(400, 'Hint does not exists!')
    }

    if (data.type || data.userEmail) {
      return new ErrorHandler(400, 'invalid field!')
    }

    return await userRepository.save({
      ...hint,
      ...data
    })
  }
}
