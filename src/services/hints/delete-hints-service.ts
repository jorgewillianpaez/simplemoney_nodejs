import { getRepository } from 'typeorm'
import { Hints } from '../../entities/Hints'
import { ErrorHandler } from '../../errors/errors'

export class DeleteHintService {
  async execute (id: string): Promise<any> {
    const repo = getRepository(Hints)
    if (!(await repo.findOne(id))) {
      return new ErrorHandler(400, 'Category does not exists!')
    }
    await repo.delete(id)
    return 'Hint deleted!'
  }
}
