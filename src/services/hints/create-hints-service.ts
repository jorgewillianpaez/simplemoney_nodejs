import { getCustomRepository, getRepository } from 'typeorm'
import { Hints } from '../../entities/Hints'
import { ErrorHandler } from '../../errors/errors'
import { Validation } from '../../protocols'
import { UserRepository } from '../../repositories/userRepository'
import { RequiredFieldValidation, ValidationComposite } from '../../validation'

interface HintsProps {
  title: string
  body: string
  references: string
  type: string
  userEmail: string
}

export class CreateHintsService {
  async execute ({ title, body, references, type, userEmail }: HintsProps): Promise<Hints | Error> {
    const repo = getRepository(Hints)
    const userRepo = getCustomRepository(UserRepository)
    const user = await userRepo.findByEmail(userEmail)
    if (!user) {
      return new ErrorHandler(400, 'User does not exists!')
    }
    if (await repo.findOne({ title })) {
      return new ErrorHandler(400, 'Hint already exists!')
    }
    const hints = repo.create({
      title,
      body,
      references,
      type,
      user_email: userEmail,
      user
    })
    await repo.save(hints)
    return hints
  }
}

export const validateHintData = (): Validation => {
  const validationsField: Validation[] = []

  for (const field of ['title', 'body', 'type', 'references', 'userEmail']) {
    validationsField.push(new RequiredFieldValidation(field))
  }

  return new ValidationComposite(validationsField)
}
