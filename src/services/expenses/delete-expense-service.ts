import { getCustomRepository } from 'typeorm'
import { ExpenseRepository } from '../../repositories/expenseRepository'

export class DeleteExpenseService {
  async execute (uuid: string): Promise<any | undefined> {
    try {
      const expenseRepository = getCustomRepository(ExpenseRepository)
      const expense = await expenseRepository.findOne(uuid)
      if (!expense) {
        return undefined
      }
      const deletedExpense = await expenseRepository.delete(uuid)
      return deletedExpense
    } catch (error) {
      return undefined
    }
  }
}
