import { getCustomRepository } from 'typeorm'
import { ExpenseRepository } from '../../repositories/expenseRepository'
import { Expenses } from '../../entities'
import { RequiredFieldValidation, ValidationComposite } from '../../validation'
import { Validation } from '../../protocols'

export class CreateExpenseService {
  async execute (data: {}): Promise<Expenses | undefined> {
    try {
      const expenseRepository = getCustomRepository(ExpenseRepository)
      const expense = expenseRepository.create(data)
      await expenseRepository.save(expense)
      return expense
    } catch (error) {
      return undefined
    }
  }
}

export const validateExpenseData = (): Validation => {
  const validationsField: Validation[] = []

  for (const field of ['name', 'value', 'actual_quota', 'quota']) {
    validationsField.push(new RequiredFieldValidation(field))
  }

  return new ValidationComposite(validationsField)
}
