import { ExpenseRepository } from '../../repositories/expenseRepository'
import { getCustomRepository } from 'typeorm'
import { Expenses } from '../../entities'

export class UpdateExpenseService {
  async execute (uuid: string, data: {}): Promise<Expenses | undefined> {
    try {
      const expenseRepository = getCustomRepository(ExpenseRepository)
      const expense = await expenseRepository.findOne(uuid)
      if (!expense) {
        return undefined
      }
      const expenseUpdated = await expenseRepository.save({
        ...expense,
        ...data
      })
      return expenseUpdated
    } catch (err) {
      return undefined
    }
  }
}
