import { getCustomRepository } from 'typeorm'
import { Expenses } from '../../entities'
import { ExpenseRepository } from '../../repositories/expenseRepository'

export class GetExpensesService {
  async execute (): Promise<Expenses[] | undefined> {
    try {
      const expenseRepository = getCustomRepository(ExpenseRepository)
      const expenses = await expenseRepository.find()
      return expenses
    } catch (err) {
      return undefined
    }
  }
}
