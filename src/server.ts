import 'reflect-metadata'
import { createConnection } from 'typeorm'
import app from './app'

const PORT = process.env.PORT ?? 3000

createConnection()
  .then(() => {
    app.listen(PORT, () => {
      console.log('Running at port 3000')
    })
  })
  .catch((error) => {
    console.log(error)
  })
