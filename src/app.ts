import express from 'express'
import { initializerRouter } from './routes'
import { handleError } from './errors/errors'
import { corsConfig } from './middlewares/cors-middleware'

const app = express()

app.use(express.json())
app.use(corsConfig)
initializerRouter(app)
app.use((err: any, req: any, res: any, next: any): any => {
  return handleError(err, res)
})
export default app
